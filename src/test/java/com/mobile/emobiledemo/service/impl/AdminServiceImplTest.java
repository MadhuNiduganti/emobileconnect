package com.mobile.emobiledemo.service.impl;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.mobile.emobiledemo.dto.RequestStatusUpdateInput;
import com.mobile.emobiledemo.dto.ResponseDto;
import com.mobile.emobiledemo.entity.Admin;
import com.mobile.emobiledemo.entity.ConnectionRequest;
import com.mobile.emobiledemo.repository.AdminRepository;
import com.mobile.emobiledemo.repository.ConnectionRequestRepository;
import com.mobile.emobiledemo.utility.EConnectionStatus;

@SpringBootTest
class AdminServiceImplTest {
	
	@Mock
	AdminRepository adminRepository;
	@Mock
	ConnectionRequestRepository connectionRequestRepository;

	@InjectMocks
	AdminServiceImpl adminServiceImpl;
	
	Admin admin;
	ConnectionRequest connectionRequest;
	RequestStatusUpdateInput requestStatusUpdateInput;
	@BeforeEach
	void setUp() throws Exception {
		admin=new Admin();
		connectionRequest=new ConnectionRequest();
		connectionRequest.setConnectionStatus(EConnectionStatus.INPROGRESS.name());
		
		requestStatusUpdateInput=new RequestStatusUpdateInput();
		requestStatusUpdateInput.setConnectionStatus(EConnectionStatus.APPROVED.name());
}

	@Test
	void testUpadateRequestStatus() {
		Mockito.when(adminRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(admin));
		Mockito.when(connectionRequestRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(connectionRequest));

	Optional<ResponseDto> result = adminServiceImpl.upadateRequestStatus(1, 1,requestStatusUpdateInput);
		assertNotNull(result);
		
	}
	
	@Test
	void testUpadateRequestStatusInvalidConnectionId() {
		Mockito.when(adminRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(admin));
try {
	 adminServiceImpl.upadateRequestStatus(Mockito.anyInt(), Mockito.anyInt(),requestStatusUpdateInput);
}catch(Exception e) {
	
}
		
	}
	
	

	@Test
	void testUpadateRequestStatusInvalidAdmin() {
try {
	 adminServiceImpl.upadateRequestStatus(Mockito.anyInt(), Mockito.anyInt(),requestStatusUpdateInput);
}catch(Exception e) {
	
}
		
	}

	
	@Test
	void testUpadateRequestStatusInvalidStatus() {
		requestStatusUpdateInput.setConnectionStatus("kkkkkk");
		Mockito.when(adminRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(admin));
		Mockito.when(connectionRequestRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(connectionRequest));

		try {
			 adminServiceImpl.upadateRequestStatus(Mockito.anyInt(), Mockito.anyInt(),requestStatusUpdateInput);
		}catch(Exception e) {
			
		}
	}
	
}
