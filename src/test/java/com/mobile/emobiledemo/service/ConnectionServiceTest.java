package com.mobile.emobiledemo.service;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.mobile.emobiledemo.dto.CustomerConnectionResponseDto;
import com.mobile.emobiledemo.dto.RequestedConnectionResponseDto;
import com.mobile.emobiledemo.entity.Admin;
import com.mobile.emobiledemo.entity.ConnectionRequest;
import com.mobile.emobiledemo.exception.InvalidUser;
import com.mobile.emobiledemo.repository.AdminRepository;
import com.mobile.emobiledemo.repository.ConnectionRequestRepository;
import com.mobile.emobiledemo.service.impl.ConnectionServiceImpl;

@ExtendWith(MockitoExtension.class)
@TestInstance(Lifecycle.PER_CLASS)
public class ConnectionServiceTest {

	private static final Integer ADMIN_ID = 1;
	private static final Integer CUSTOMER_ID = 1;
	private static final Integer CONNECTION_REQUEST_ID = 1;
	@Mock
	ConnectionRequestRepository connectionRepository;

	@Mock
	AdminRepository adminRepository;

	@InjectMocks
	ConnectionServiceImpl connectionServiceImpl;

	ConnectionRequest connectionRequest;
	List<ConnectionRequest> listOfRequest;
	Admin admin;

	@BeforeAll
	public void setup() {
		listOfRequest = new ArrayList<ConnectionRequest>();
		connectionRequest = new ConnectionRequest();
		connectionRequest.setConnectionRequestId(1);
		connectionRequest.setConnectionStatus("In Progress");
		connectionRequest.setCustomerId(1);
		listOfRequest.add(connectionRequest);

		admin = new Admin();
		admin.setAdminId(1);

	}

	@Test
	@DisplayName("Getting list of In progress requests")
	public void viewRequestedListTest() {
		when(adminRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(admin));
		when(connectionRepository.findByConnectionStatus(Mockito.anyString())).thenReturn(Optional.of(listOfRequest));

		// WHEN
		RequestedConnectionResponseDto actual = connectionServiceImpl.viewAllNewRequest(ADMIN_ID);

		// THEN
		Assertions.assertEquals(actual.getListOfRequests().get(0).getConnectionRequestId(),
				listOfRequest.get(0).getConnectionRequestId());
	}
	

	@Test
	@DisplayName("Getting Status of the connection request")
	public void viewConnectionStatus() {
		when(connectionRepository.findByCustomerIdAndConnectionRequestId(Mockito.anyInt(), Mockito.anyInt()))
				.thenReturn(Optional.of(connectionRequest));

		// WHEN
		CustomerConnectionResponseDto actual = connectionServiceImpl.getConnectionRequestStatus(CUSTOMER_ID,
				CONNECTION_REQUEST_ID);

		// THEN
		Assertions.assertEquals(actual.getConnectionStatus(),connectionRequest.getConnectionStatus());
	}
}
