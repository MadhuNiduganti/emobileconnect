package com.mobile.emobiledemo.controller;

import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import com.mobile.emobiledemo.dto.CustomerConnectionResponseDto;
import com.mobile.emobiledemo.service.impl.ConnectionServiceImpl;

@ExtendWith(MockitoExtension.class)
@TestInstance(Lifecycle.PER_CLASS)
public class CustomerControllerTest {

	private static final Integer CUSTOMER_ID = 1;
	private static final Integer CONNECTION_REQUEST_ID = 1;
	@Mock
	ConnectionServiceImpl connectionServiceImpl;

	@InjectMocks
	CustomerController customerController;

	CustomerConnectionResponseDto customerConnectionResponseDto;

	@BeforeAll
	public void setup() {
		customerConnectionResponseDto = new CustomerConnectionResponseDto();
		customerConnectionResponseDto.setConnectionStatus("ACCEPT");
		customerConnectionResponseDto.setStatusCode(200);
	}

	@Test
	@DisplayName("Getting connection status of the request")
	public void viewRequestedListTest() {

		// WHEN
		when(connectionServiceImpl.getConnectionRequestStatus(Mockito.anyInt(), Mockito.anyInt()))
				.thenReturn(customerConnectionResponseDto);
		ResponseEntity<CustomerConnectionResponseDto> actual = customerController
				.getConnectionRequestStatus(CUSTOMER_ID, CONNECTION_REQUEST_ID);
		// THEN
		Assertions.assertEquals(actual.getBody().getConnectionStatus(), customerConnectionResponseDto.getConnectionStatus());

	}
}
