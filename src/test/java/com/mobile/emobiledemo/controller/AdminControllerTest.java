
package com.mobile.emobiledemo.controller;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import com.mobile.emobiledemo.dto.RequestedConnectionResponseDto;
import com.mobile.emobiledemo.dto.ViewRequestResponseDto;
import com.mobile.emobiledemo.service.impl.ConnectionServiceImpl;

@ExtendWith(MockitoExtension.class)
@TestInstance(Lifecycle.PER_CLASS)
public class AdminControllerTest {

	private static final Integer ADMIN_ID = 1;

	@Mock
	ConnectionServiceImpl connectionServiceImpl;

	@InjectMocks
	AdminController adminController;

	RequestedConnectionResponseDto response;

	ViewRequestResponseDto viewRequestResponseDto;

	List<ViewRequestResponseDto> listViewRequestResponseDto;

	@BeforeAll
	public void setup() {
		viewRequestResponseDto = new ViewRequestResponseDto();
		viewRequestResponseDto.setConnectionStatus("In Progress");
		viewRequestResponseDto.setIdentityProofNumber("6524361764");
		viewRequestResponseDto.setTalktimePlanId(1);
		viewRequestResponseDto.setConnectionRequestId(1);

		listViewRequestResponseDto = new ArrayList<ViewRequestResponseDto>();
		listViewRequestResponseDto.add(viewRequestResponseDto);

		response = new RequestedConnectionResponseDto();
		response.setListOfRequests(listViewRequestResponseDto);
		response.setStatusCode(200);
	}

	@Test
	@DisplayName("Getting list of In progress requests")
	public void viewRequestedListTest() {

		// WHEN
		when(connectionServiceImpl.viewAllNewRequest(Mockito.anyInt())).thenReturn(response);
		ResponseEntity<RequestedConnectionResponseDto> actual = adminController.viewAllNewRequest(ADMIN_ID);
		// THEN
		Assertions.assertEquals(actual.getBody().getStatusCode(), response.getStatusCode());
		Assertions.assertEquals(actual.getBody().getListOfRequests().get(0).getConnectionRequestId(),
				response.getListOfRequests().get(0).getConnectionRequestId());
	}
}
