package com.mobile.emobiledemo.service;

import java.util.Optional;

import com.mobile.emobiledemo.dto.RequestStatusUpdateInput;
import com.mobile.emobiledemo.dto.ResponseDto;

public interface AdminService {
	
	Optional<ResponseDto> upadateRequestStatus(Integer adminId, Integer connectionId, RequestStatusUpdateInput requestStatusUpdateInput);

}
