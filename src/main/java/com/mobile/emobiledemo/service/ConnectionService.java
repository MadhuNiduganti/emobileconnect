package com.mobile.emobiledemo.service;

import com.mobile.emobiledemo.dto.ConnectionReqDto;
import com.mobile.emobiledemo.dto.ConnectionResDto;
import com.mobile.emobiledemo.dto.CustomerConnectionResponseDto;
import com.mobile.emobiledemo.dto.RequestedConnectionResponseDto;
import com.mobile.emobiledemo.exception.ConnectionException;

public interface ConnectionService {

	RequestedConnectionResponseDto  viewAllNewRequest(Integer adminId);
	void updateConnectionStatus() ;


	CustomerConnectionResponseDto getConnectionRequestStatus(Integer customerId, Integer connectionRequestId);

	ConnectionResDto connReq(ConnectionReqDto connectionReqDto, Integer customerId) throws ConnectionException;

}
