package com.mobile.emobiledemo.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mobile.emobiledemo.dto.RequestStatusUpdateInput;
import com.mobile.emobiledemo.dto.ResponseDto;
import com.mobile.emobiledemo.entity.ConnectionRequest;
import com.mobile.emobiledemo.exception.InvalidUser;
import com.mobile.emobiledemo.repository.AdminRepository;
import com.mobile.emobiledemo.repository.ConnectionRequestRepository;
import com.mobile.emobiledemo.service.AdminService;
import com.mobile.emobiledemo.utility.EConnectionStatus;

@Service
public class AdminServiceImpl implements AdminService {

	@Autowired
	AdminRepository adminRepository;
	@Autowired
	ConnectionRequestRepository connectionRequestRepository;

	@Override
	public Optional<ResponseDto> upadateRequestStatus(Integer adminId, Integer connectionId,RequestStatusUpdateInput requestStatusUpdateInput) {

		if (!adminRepository.findById(adminId).isPresent()) {
			throw new InvalidUser("in valid admin credintials");
		}

		 Optional<ConnectionRequest> connectionRequestOptional = connectionRequestRepository.findById(connectionId);
		if (!connectionRequestOptional.isPresent()) {
			throw new InvalidUser("in valid requestId credintials");
		}
		
		if(!connectionRequestOptional.get().getConnectionStatus().equalsIgnoreCase(EConnectionStatus.INPROGRESS.name())){
			throw new InvalidUser("in valid requestId credintials");

		}
		
		try {
			EConnectionStatus.valueOf(requestStatusUpdateInput.getConnectionStatus());
		}catch(Exception e) {
			throw new InvalidUser("in valid status");

		}
		
		
		
		connectionRequestOptional.get().setConnectionStatus(EConnectionStatus.valueOf(requestStatusUpdateInput.getConnectionStatus()).name());
		connectionRequestRepository.save(connectionRequestOptional.get());
		
		ResponseDto responseDto=new ResponseDto();
		responseDto.setMessage("connection approved successfully");
		responseDto.setStatusCode("601");
		return Optional.of(responseDto);

	}

}
