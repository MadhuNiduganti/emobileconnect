package com.mobile.emobiledemo.service.impl;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mobile.emobiledemo.dto.ConnectionReqDto;
import com.mobile.emobiledemo.dto.ConnectionResDto;
import com.mobile.emobiledemo.dto.CustomerConnectionResponseDto;
import com.mobile.emobiledemo.dto.RequestedConnectionResponseDto;
import com.mobile.emobiledemo.dto.ViewRequestResponseDto;
import com.mobile.emobiledemo.entity.Admin;
import com.mobile.emobiledemo.entity.ConnectionRequest;
import com.mobile.emobiledemo.entity.Customer;
import com.mobile.emobiledemo.entity.TalktimePlan;
import com.mobile.emobiledemo.exception.ConnectionException;
import com.mobile.emobiledemo.exception.InvalidCustomerAndConnectionRequestId;
import com.mobile.emobiledemo.exception.InvalidUser;
import com.mobile.emobiledemo.repository.AdminRepository;
import com.mobile.emobiledemo.repository.ConnectionRequestRepository;
import com.mobile.emobiledemo.repository.CustomerRepository;
import com.mobile.emobiledemo.repository.TalktimePlanRepository;
import com.mobile.emobiledemo.service.ConnectionService;

import com.mobile.emobiledemo.utility.ConnectionStatus;

import com.mobile.emobiledemo.utility.EConnectionStatus;

import com.mobile.emobiledemo.utility.ErrorConstants;
import com.mobile.emobiledemo.utility.MobileNumberGenerator;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ConnectionServiceImpl implements ConnectionService, Serializable {
	private static final long serialVersionUID = 1L;

	private final static String status = "INPROGRESS";

	@Autowired
	TalktimePlanRepository talktimePlanRepository;
	@Autowired
	CustomerRepository customerRepository;
	
	@Autowired
	ConnectionRequestRepository connectionRequestRepository;


	@Autowired
	AdminRepository adminRepository;

	/**
	 * @author SubhaM
	 *         This method will fetch all the InProgress requests for the admin view
	 * 
	 * @return RequestedConnectionResponseDto - list of InProgress connection
	 *         requests,message and status code
	 */
	@Override
	public RequestedConnectionResponseDto viewAllNewRequest(Integer adminId) {
		log.info("Inside viewAllNewRequest method");
		Optional<Admin> adminuser = adminRepository.findById(adminId);

		if (!(adminuser.isPresent())) {
			throw new InvalidUser(ErrorConstants.INVALID_CUSTOMER);
		}
		Optional<List<ConnectionRequest>> listOfConnections = connectionRequestRepository.findByConnectionStatus(status);

		List<ViewRequestResponseDto> listProductResponseDto = listOfConnections.get().stream().map(listOfrequest -> {
			ViewRequestResponseDto productResponseDto = new ViewRequestResponseDto();
			BeanUtils.copyProperties(listOfrequest, productResponseDto);
			return productResponseDto;
		}).collect(Collectors.toList());
		RequestedConnectionResponseDto responseRequestsData = new RequestedConnectionResponseDto();
		responseRequestsData.setListOfRequests(listProductResponseDto);
		responseRequestsData.setMessage("requested connections are in progress fetched successfully");
		responseRequestsData.setStatusCode(200);
		return responseRequestsData;
	}

	@Override
	public void updateConnectionStatus() {
		
		log.info("RequestedConnectionResponseDto-->updateConnectionStatus : {} ",LocalDateTime.now());

		Optional<List<ConnectionRequest>> connectionRequestListOptional = connectionRequestRepository
				.findByConnectionStatus(EConnectionStatus.APPROVED.name());

		connectionRequestListOptional.ifPresent(connectionRequestList -> {

			connectionRequestList.stream().forEach(connectionRequest -> {
				connectionRequest.setConnectionStatus(EConnectionStatus.ACTIVE.name());
				connectionRequest.setMobileNumber(generateMobileNumber());
				connectionRequestRepository.save(connectionRequest);

			});
		});
	}

	private Long generateMobileNumber() {
		long accountNumber = MobileNumberGenerator.numbergeneration();
		Optional<List<ConnectionRequest>> connectionRequests = connectionRequestRepository
				.findByMobileNumber(accountNumber);
		if (connectionRequests.isPresent()) {
			generateMobileNumber();
		}

		return accountNumber;
	}
	/**
	 * @author SubhaM
	 * 
	 *         This method will fetch the status of the requested connection for
	 *         user
	 * 
	 *         customerId,connectionRequestId will be the inputs in path variable
	 * 
	 * @return CustomerConnectionResponseDto - Connection status and status code,
	 *         message
	 */
	@Override
	public CustomerConnectionResponseDto getConnectionRequestStatus(Integer customerId, Integer connectionRequestId) {

		log.info("Inside getConnectionRequestStatus method" + customerId + "" + connectionRequestId);
		
		Optional<ConnectionRequest> response = connectionRequestRepository.findByCustomerIdAndConnectionRequestId(customerId,
					connectionRequestId);

		if (!(response.isPresent())) {
			throw new InvalidCustomerAndConnectionRequestId(ErrorConstants.INVALID_CUSTOMER_AND_CONNECTION_ID);
		}

		CustomerConnectionResponseDto responseData = new CustomerConnectionResponseDto();
		responseData.setConnectionStatus(response.get().getConnectionStatus());
		responseData.setMessage("Satus of the connection request fetched successfully");
		responseData.setStatusCode(200);
		return responseData;

	}

	 @Override
	public ConnectionResDto  connReq(ConnectionReqDto connectionReqDto, Integer customerId) throws  ConnectionException{
		
		 ConnectionRequest connectionRequest= new ConnectionRequest();
		 ConnectionResDto connectionResDto = new ConnectionResDto();
		 
		 BeanUtils.copyProperties(connectionReqDto, connectionRequest);
		
		 System.out.println(connectionReqDto.getIdentityProofNumber());
		 System.out.println(connectionReqDto.getIdentityProofNumber().length());
		 System.out.println(connectionReqDto.getIdentityProofNumber().length()==12);
		 
		 if(!(connectionReqDto.getIdentityProofNumber().length()==12)) {
			throw new ConnectionException(ErrorConstants.INVAL_ID_PROOF);
		 }	
		Optional<Customer> customer=customerRepository.findById(customerId);
		if(!customer.isPresent()) {
			throw new ConnectionException(ErrorConstants.INVAL_CUSTOMER_ID);
		}else {
			connectionRequest.setCustomerId(customer.get().getCustomerId());
		}
		Optional<TalktimePlan> talktimePlan=talktimePlanRepository.findById(connectionReqDto.getTalktimePlanId());
		
		if(!talktimePlan.isPresent()) {
			throw new ConnectionException(ErrorConstants.INVAL_PLAN_ID);
		}
		connectionRequest.setConnectionStatus(ConnectionStatus.INPROGRESS.name());
		
		connectionRequestRepository.save(connectionRequest);
		
		connectionResDto.setConnectionRequestId(connectionRequest.getConnectionRequestId());
		connectionResDto.setMessage(ErrorConstants.REQ_CREATED_SUCC);
		connectionResDto.setStatusCode(ErrorConstants.REQ_CREATED_SUCC_CODE);
		
		return connectionResDto;
	
	 }

}
