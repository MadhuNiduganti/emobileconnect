package com.mobile.emobiledemo.exception;

import java.io.Serializable;

public class InvalidUser extends RuntimeException implements Serializable{

	private static final long serialVersionUID = 1L;

		public InvalidUser(String message) {
			super(message);
		}
}
