package com.mobile.emobiledemo.exception;

public class ConnectionException extends Exception{
	

	private static final long serialVersionUID = -1747502351308293745L;

	public ConnectionException(String message) {
		super(message);
	}
}
