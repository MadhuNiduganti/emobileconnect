package com.mobile.emobiledemo.exception;

import java.io.Serializable;

public class InvalidCustomerAndConnectionRequestId extends RuntimeException implements Serializable{

	private static final long serialVersionUID = 1L;

		public InvalidCustomerAndConnectionRequestId(String message) {
			super(message);
		}

}
