package com.mobile.emobiledemo;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.mobile.emobiledemo.service.ConnectionService;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@EnableScheduling
@Slf4j
public class EmobiledemoApplication {

	@Autowired
	ConnectionService connectionService;
	public static void main(String[] args) {
		SpringApplication.run(EmobiledemoApplication.class, args);
	}
	
	/**
	 * @author allam.sairam
	 */
	@Scheduled(fixedDelay = 20000)
	public void scheduler() {
		log.info("scheduler ruininng : {}", LocalDateTime.now());
		
		connectionService.updateConnectionStatus();
	}
	

}
