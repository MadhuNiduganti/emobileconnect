package com.mobile.emobiledemo.utility;

public enum ConnectionStatus {
	INPROGRESS, APPROVED, REJECTED, ACTIVE
}
