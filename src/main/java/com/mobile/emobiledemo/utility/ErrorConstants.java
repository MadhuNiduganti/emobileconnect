package com.mobile.emobiledemo.utility;

public class ErrorConstants {
	
	private ErrorConstants() {

	}
	public static final String INVALID_CUSTOMER = "Not an admin user";
	public static final String INVALID_CUSTOMER_AND_CONNECTION_ID = "No data available for this customer id and connection request id ";
	public static final String REQ_CREATED_SUCC = "Connection request created successfuly";
	public static final int REQ_CREATED_SUCC_CODE = 200;
	
	public static final String INVAL_ID_PROOF = "Identity Proof Number is invalid";
	public static final String INVAL_CUSTOMER_ID = "Customer id is invalid";
	public static final String INVAL_PLAN_ID = "ConnectionPlan id is invalid";
}
