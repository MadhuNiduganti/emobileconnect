package com.mobile.emobiledemo.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerConnectionResponseDto {
	
	private String connectionStatus;
	private String message;
	private Integer statusCode;
}
