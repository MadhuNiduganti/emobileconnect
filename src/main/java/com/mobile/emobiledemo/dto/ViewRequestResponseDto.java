package com.mobile.emobiledemo.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ViewRequestResponseDto {
	
	private Integer connectionRequestId;
	private String connectionStatus;
	private String address;
	private String identityProofNumber;
	private Integer talktimePlanId;

}
