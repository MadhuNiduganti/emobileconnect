package com.mobile.emobiledemo.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestedConnectionResponseDto {
	private String message;
	private Integer statusCode;
	List<ViewRequestResponseDto> listOfRequests;
	
}
