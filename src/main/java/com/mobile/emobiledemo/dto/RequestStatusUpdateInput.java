package com.mobile.emobiledemo.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RequestStatusUpdateInput {
	private String connectionStatus;
}
