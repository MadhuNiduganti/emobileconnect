package com.mobile.emobiledemo.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConnectionResDto {
	private Integer connectionRequestId;
	private Integer statusCode;
	private String message;
}
