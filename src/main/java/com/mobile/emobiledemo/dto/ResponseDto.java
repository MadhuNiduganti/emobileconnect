package com.mobile.emobiledemo.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ResponseDto {
	
	private String message;
	private String statusCode;

}
