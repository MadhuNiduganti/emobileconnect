package com.mobile.emobiledemo.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class ConnectionReqDto {

	private String address;
	private String identityProofNumber;
	private Integer talktimePlanId;
}
