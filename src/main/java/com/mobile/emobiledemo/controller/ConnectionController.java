package com.mobile.emobiledemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mobile.emobiledemo.dto.ConnectionReqDto;
import com.mobile.emobiledemo.dto.ConnectionResDto;
import com.mobile.emobiledemo.exception.ConnectionException;
import com.mobile.emobiledemo.service.ConnectionService;

@RestController
public class ConnectionController {
	

	@Autowired
	ConnectionService connectionService;

	@PostMapping("/customers/{customerId}/connections")
	public ResponseEntity<ConnectionResDto> connReq(@RequestBody ConnectionReqDto connectionReqDto,
			@PathVariable("customerId") Integer customerId) throws ConnectionException {
		ConnectionResDto connectionResDto = connectionService.connReq(connectionReqDto, customerId);
		return new ResponseEntity<>(connectionResDto, HttpStatus.OK);
	}
}
