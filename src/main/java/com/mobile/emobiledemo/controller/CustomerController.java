package com.mobile.emobiledemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mobile.emobiledemo.dto.CustomerConnectionResponseDto;
import com.mobile.emobiledemo.service.ConnectionService;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author SubhaM
 *
 *         This class will do the Customer role related operations
 */
@RestController
@RequestMapping("/customers")
@Slf4j
public class CustomerController {

	@Autowired
	ConnectionService connectionService;

	/**
	 * 
	 * @author SubhaM
	 *
	 *         This method will fetch all the InProgress requests for the admin view
	 *         customerId,connectionRequestId will be the inputs in path variable
	 * @return RequestedConnectionResponseDto - list of InProgress connection
	 *         requests,message and status code
	 */
	@GetMapping("/{customerId}/connections/{connectionRequestId}/status")
	public ResponseEntity<CustomerConnectionResponseDto> getConnectionRequestStatus(@PathVariable Integer customerId,
			@PathVariable Integer connectionRequestId) {
		log.info("Inside getConnectionRequestStatus method");
		CustomerConnectionResponseDto response = connectionService.getConnectionRequestStatus(customerId,
				connectionRequestId);
		return new ResponseEntity<CustomerConnectionResponseDto>(response, HttpStatus.OK);
	}
}
