package com.mobile.emobiledemo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mobile.emobiledemo.dto.RequestStatusUpdateInput;
import com.mobile.emobiledemo.dto.RequestedConnectionResponseDto;
import com.mobile.emobiledemo.dto.ResponseDto;
import com.mobile.emobiledemo.service.AdminService;
import com.mobile.emobiledemo.service.ConnectionService;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author SubhaM 
 * @author allam.sairam
 *
 *         This class will do the Admin role related operations
 */
@RestController
@RequestMapping("/admin")
@Slf4j
public class AdminController {
	@Autowired
	AdminService adminService;
	@Autowired
	ConnectionService connectionService;

	/**
	 * 
	 * @author allam.sairam
	 *
	 *@apiNote  This method will upadte the staus of new request
	 * 
	 * @return ResponseDto -message and status code
	 */
	@PutMapping("/{adminId}/connections/{connectionRequestId}")
	public ResponseEntity<Optional<ResponseDto>> updateConnectionStatus(@PathVariable("adminId") Integer adminId,
			@PathVariable("connectionRequestId") Integer connectionId, @RequestBody  RequestStatusUpdateInput requestStatusUpdateInput) {
		return ResponseEntity.status(HttpStatus.CREATED).body(adminService.upadateRequestStatus(adminId, connectionId,requestStatusUpdateInput));

	}


	/**
	 * 
	 * @author SubhaM
	 *
	 *         This method will fetch all the InProgress requests for the admin view
	 * 
	 * @return RequestedConnectionResponseDto - list of InProgress connection
	 *         requests,message and status code
	 */
	@GetMapping("/{adminId}/connections")
	public ResponseEntity<RequestedConnectionResponseDto> viewAllNewRequest(@PathVariable Integer adminId) {
		log.info("Inside view request method");
		RequestedConnectionResponseDto response = connectionService.viewAllNewRequest(adminId);
		return new ResponseEntity<RequestedConnectionResponseDto>(response, HttpStatus.OK);
	}
}
