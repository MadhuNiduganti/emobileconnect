package com.mobile.emobiledemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mobile.emobiledemo.entity.TalktimePlan;

public interface TalktimePlanRepository extends JpaRepository<TalktimePlan, Integer> {

}
