package com.mobile.emobiledemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mobile.emobiledemo.entity.Customer;

public interface CustomerRepostory extends JpaRepository<Customer, Integer> {

}
