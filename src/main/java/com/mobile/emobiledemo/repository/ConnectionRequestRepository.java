package com.mobile.emobiledemo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mobile.emobiledemo.entity.ConnectionRequest;

@Repository
public interface ConnectionRequestRepository extends JpaRepository<ConnectionRequest, Integer> {
	Optional<List<ConnectionRequest>> findByConnectionStatus(String status);

	Optional<List<ConnectionRequest>> findByMobileNumber(Long mobileNumber);

	Optional<ConnectionRequest> findByCustomerIdAndConnectionRequestId(Integer customerId, Integer connectionRequestId);

}
