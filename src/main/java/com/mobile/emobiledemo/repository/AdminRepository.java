package com.mobile.emobiledemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mobile.emobiledemo.entity.Admin;

@Repository
public interface AdminRepository extends JpaRepository<Admin, Integer> {


}
