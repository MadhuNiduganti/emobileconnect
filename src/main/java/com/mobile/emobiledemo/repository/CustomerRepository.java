package com.mobile.emobiledemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mobile.emobiledemo.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {

}
