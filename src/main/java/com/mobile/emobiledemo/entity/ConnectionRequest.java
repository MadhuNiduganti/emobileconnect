package com.mobile.emobiledemo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
@Table
public class ConnectionRequest {
		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		private Integer connectionRequestId;
		private String connectionStatus;
		private String address;
		private Integer customerId;
		private String identityProofNumber;
		private Integer talktimePlanId;
		private Long mobileNumber;

}
