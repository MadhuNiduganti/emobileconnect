package com.mobile.emobiledemo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
public class TalktimePlan {
		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		private Integer talktimePlanId;
		private String talktimePlanName;
		private Integer price;
		
}
